This `testing.h` is a simple unit test framework for C and C++ projects. It needs at least C99.

Documentation is included in the file.

There is a small showcase of the features in the `example` directory.

License: MIT
