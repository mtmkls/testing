// tests for DUT
// license: WTFPL

#include "testing.h"

#include "dut.h"

#include <stdlib.h>
#include <string.h>

TEST_INIT("Device Under Test");

// showcase the features of testing.h

static void test_create(void)
{
    struct Device *d1 = new_device("salty");
    OK_FATAL(d1 != NULL, "have device");
    struct Device *d2 = new_device("salty");
    OK_FATAL(d2 != NULL, "have device");

    OK(d1 != d2, "objects should be different %p %p", d1, d2);

    OK(delete_device(d1) == NULL, "should return NULL");
    OK(delete_device(d2) == NULL, "should return NULL");
    OK(delete_device(NULL) == NULL, "can delete NULL");
}

static void test_name(void)
{
    char *s = (char*)malloc(10*sizeof(char));
    strcpy(s, "velocity");
    struct Device *d = new_device(s);
    OK_FATAL(d != NULL, "have device");

    OK(s != device_drive(d, 3), "should return different pointer");
    OK(device_drive(d, 7) == device_drive(d, 3), "should always return the same pointer");

    strcpy(s, "slow"); // should not affect the name of d
    OK(strcmp(device_drive(d, 2), "velocity") == 0, "name '%s", device_drive(d, 2));
    free(s); // should not affect the name of d
    OK(strcmp(device_drive(d, 2), "velocity") == 0, "name '%s", device_drive(d, 2));

    OK(delete_device(d) == NULL, "should return NULL");
}

static void test_skip(void)
{
    SKIP("nothing to see here");
    FAIL("should not reach this");
}

static void test_drive(void)
{
    struct Device *d = new_device("driving");

    OK(device_rounds(d) == 0, "initial value %u", device_rounds(d));

    // the function doesn't assert so it's a test failure
    ASSERTS(device_drive(d, 2), "actually it's okay to not assert");

    // here we expect the assert so it is not a test failure
    ASSERTS(device_rounds(d), "did not assert??");

    // the assert is a fatal test failure here
    OK(device_rounds(d) == 2, "value %u", device_rounds(d));

    FAIL("we don't reach this because of the assert");

    OK(delete_device(d) == NULL, "should return NULL");
}

TEST_CASES = {
    {"create", test_create},
    {"name", test_name},
    {"skip", test_skip},
    {"drive", test_drive},
    {NULL, NULL}
};
