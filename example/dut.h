// Device Under Test
// license: WTFPL

#ifndef DEVICE_UNDER_TEST_H
#define DEVICE_UNDER_TEST_H

struct Device;

// makes a copy of @name
struct Device *new_device(const char *name);

// always returns NULL
struct Device *delete_device(struct Device *dev);

// returns the number of rounds
unsigned device_rounds(const struct Device *dev);

// increases the rounds and returns the name
const char *device_drive(struct Device *dev, unsigned speed);

#endif // DEVICE_UNDER_TEST_H
