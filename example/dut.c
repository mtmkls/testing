// Device Under Test
// license: WTFPL

#include "dut.h"

#include <stdlib.h>
#include <string.h>

// this is all no-op when TESTING is not defined
#include "testing.h"

struct Device {
    char *name;
    unsigned rounds;
};

struct Device *new_device(const char *name)
{
    struct Device *ret = (struct Device *)malloc(sizeof(struct Device));
    unsigned len = strlen(name) + 1;
    ret->name = (char *)malloc(len*sizeof(char));
    strcpy(ret->name, name);
    ret->rounds = 0;
    return ret;
}

struct Device *delete_device(struct Device *dev)
{
    if (dev == NULL)
        return NULL;

    free(dev->name);
    free(dev);
    return NULL;
}

unsigned device_rounds(const struct Device *dev)
{
    assert(dev->rounds == 0 && "should be zero");
    return dev->rounds*3;
}

const char *device_drive(struct Device *dev, unsigned speed)
{
    dev->rounds += speed;
    OK(dev->rounds == speed, "rounds %u speed %u", dev->rounds, speed);
    return dev->name;
}

#ifndef TESTING

#include <stdio.h>

int main(void)
{
    struct Device *d = new_device("not testing now");
    OK_FATAL(d == NULL, "this is no-op");
    printf("device rounds %u\n", device_rounds(d)); // the assert passes
    printf("device %s\n", device_drive(d, 2));
    printf("device %s\n", device_drive(d, 2)); // the OK() in this call would fail
    OK(delete_device(d), "not deleting because no-op");
    delete_device(d);
}

#endif

