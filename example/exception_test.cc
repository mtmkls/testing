// showcase of the C++ features
// license: WTFPL

#include "testing.h"

TEST_INIT("C++ exception test");

static void nothrows() {}

static void throwint()
{ throw 42; }

static void throwstring()
{ throw "string"; }

static void test_asserts()
{
    ASSERTS(assert(0), "okay");
    ASSERTS(nothrows(), "fail");
    ASSERTS_FATAL(assert(1), "fail");
    FAIL("should not reach this");
}

static void test_exceptions()
{
    EXCEPTS(nothrows(), int, "fail");
    EXCEPTS(throwint(), int, "okay");
    EXCEPTS(throwint(), char*, "fail");
    EXCEPTS(throwint(), const char*, "fail");
    EXCEPTS(throwstring(), int, "fail");
    EXCEPTS(throwstring(), char*, "fail");
    EXCEPTS(throwstring(), const char*, "okay");

    throwint(); // uncaught exception: fail

    EXCEPTS(throwstring(), double, "should not reach this");
}

TEST_CASES = {
    {"asserts", test_asserts},
    {"exceptions", test_exceptions},
    {NULL, NULL}
};
